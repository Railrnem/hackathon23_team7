# Hackathon23_Team7


## Description
A small project created during the Hackathon23 from the ITECH.
This project is a small web application that allow teachers to create tasks and students to see what
tasks they have assigned. The goal is to create better time management for students and teachers.

## Installation
1. Make sure 'flask' and 'mysql-connector-python' is installed:
``` pip install flask ```
``` pip install mysql-connector-python ```
2. Import the SQL Database from the folder sql
3. Copy db-connection.py.example to db-connection.py and change the values to your database