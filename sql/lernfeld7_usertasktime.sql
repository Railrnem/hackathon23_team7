CREATE DATABASE  IF NOT EXISTS `lernfeld7` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `lernfeld7`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: lernfeld7
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usertasktime`
--

DROP TABLE IF EXISTS `usertasktime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usertasktime` (
  `USERID` int NOT NULL,
  `TASKID` int NOT NULL,
  `time` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`USERID`,`TASKID`),
  KEY `taskid_idx` (`TASKID`),
  CONSTRAINT `tasktime` FOREIGN KEY (`TASKID`) REFERENCES `tasks` (`ID`),
  CONSTRAINT `useridtime` FOREIGN KEY (`USERID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usertasktime`
--

LOCK TABLES `usertasktime` WRITE;
/*!40000 ALTER TABLE `usertasktime` DISABLE KEYS */;
INSERT INTO `usertasktime` VALUES (1,30,'0'),(1,31,'0'),(1,35,'0'),(2,30,'1'),(2,31,'0'),(2,35,'0'),(3,30,'2'),(3,31,'3'),(3,32,'0'),(3,33,'0'),(3,35,'0'),(3,36,'0'),(4,30,'0'),(4,31,'0'),(4,32,'0'),(4,33,'0'),(4,35,'0'),(4,36,'0'),(5,30,'0'),(5,31,'0'),(5,35,'0'),(6,32,'0'),(6,33,'0'),(6,36,'0'),(7,32,'0'),(7,33,'0'),(7,36,'0'),(8,30,'0'),(8,31,'0'),(8,35,'0'),(9,30,'0'),(9,31,'6'),(9,35,'0');
/*!40000 ALTER TABLE `usertasktime` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-05 10:29:25
