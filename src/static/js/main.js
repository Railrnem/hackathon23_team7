window.onload = function () {
    var hours = 00;
    var minutes = 00;
    var seconds = 00; 
    var tens = 00; 
    var appendTens = document.getElementById("tens");
    var appendSeconds = document.getElementById("seconds");
    var appendMinutes = document.getElementById("minutes");
    var appendHours = document.getElementById("hours");
    var buttonStart = document.getElementById('button-start');
    var buttonStop = document.getElementById('button-stop');
    var buttonReset = document.getElementById('button-reset');
    var Interval;
  
    // Start timer
    buttonStart.onclick = function() {
        clearInterval(Interval);
        // Calls startTimer function every 10 miliseconds
        Interval = setInterval(startTimer, 10);
    }
    
    // Stop timer
    buttonStop.onclick = function() {
        clearInterval(Interval);
    }
    
    // Reset timer to zero
    buttonReset.onclick = function () {
        clearInterval(Interval);
        tens = "00";
        seconds = "00";
        minutes = "00";
        hours = "00";
        appendTens.innerHTML = tens;
        appendSeconds.innerHTML = seconds;
        appendMinutes.innerHTML = minutes;
        appendHours.innerHTML = hours;
    }
    
    // This function, updates the timerfields every time it is called
    function startTimer() {
        tens++;

        // Check to ensure that both digits are always shown
        if (tens < 10) {
            appendTens.innerHTML = "0" + tens;
        } else {
            appendTens.innerHTML = tens;
        }
    
        // One second has passed
        if (tens > 99) {
            seconds++;
            appendSeconds.innerHTML = "0" + seconds;
            tens = 0;
            appendTens.innerHTML = "0" + tens;
        }
    
        // Check to ensure that both digits are always shown
        if (seconds < 10) {
            appendSeconds.innerHTML = "0" + seconds;
        } else {
            appendSeconds.innerHTML = seconds;
        }
    
        // One minute has passed
        if (seconds >= 60) {
            seconds = 0;
            appendSeconds.innerHTML = "0" + seconds;
            minutes++;
        }
    
        // Check to ensure that both digits are always shown
        if (minutes < 10) {
            appendMinutes.innerHTML = "0" + minutes;
        } else {
            appendMinutes.innerHTML = minutes;
        }
    
        // One hour has passed
        if (minutes >= 60) {
            minutes = 0;
            appendMinutes.innerHTML = "0" + minutes;
            hours++;
        }
    
        // Check to ensure that both digits are always shown
        if (hours < 10) {
            appendHours.innerHTML = "0" + hours;
        } else {
            appendHours.innerHTML = hours;
        }
    
        // Limit of the Timer has been reached
        if (hours > 99) {
            hours = 99;
            appendHours.innerHTML = hours;
        }
    }
    
  }

document.addEventListener("DOMContentLoaded", function () {
    // Add an event listener to the form submission
    document.querySelector("form").addEventListener("submit", function () {
        // Get the values from the <span> elements
        var hoursValue = document.getElementById("hours").textContent;
        var minutesValue = document.getElementById("minutes").textContent;
        var secondsValue = document.getElementById("seconds").textContent;

        // make sure the elements are filled
        if (hoursValue === ""){
            hoursValue = 0;
        }
        if (minutesValue === ""){
            minutesValue = 0;
        }
        if (secondsValue === ""){
            minutesValue = 0;
        }
        
        // Update the hidden input fields with the extracted values
        document.getElementById("hours_input").value = hoursValue;
        document.getElementById("minutes_input").value = minutesValue;
        document.getElementById("seconds_input").value = secondsValue;
    });
});