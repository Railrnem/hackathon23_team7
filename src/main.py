from flask import Flask, render_template, request, session, render_template_string, redirect, url_for

import database

app = Flask(__name__)

# Details on the Secret Key: https://flask.palletsprojects.com/en/2.3.x/config/#SECRET_KEY
# NOTE: The secret key is used to cryptographically-sign the cookies used for storing
#       the session data.
# NOTE: The secret key should be kept in a seperate untracked file. This is not safe!
app.secret_key = '673a529da5a41de72b2aa73da19d82be1d8fe36423a81cee0d2f3f35dfbb6b21'


# Home Page
@app.route("/")
def homepage():
    return render_template('index.html')

# Login page
@app.route("/login", methods=['GET', 'POST'])
def login():    
    # Post method: Check login data, redirect to homepage on successful login, show loginpage on unsuccesfull login
    if request.method == 'POST':
        # Read database to determine if correct password has been entered
        db = database.Database()
        result = db.getPassword(request.form['account'])
        if result[0][0] == request.form['password']:
            # Successful login
            # Save the form data to the session object
            session['account'] = request.form['account']
            result = db.getUsertype(request.form['account'])
            session['accounttyp'] = result[0][0]
            result = db.getUserId(request.form['account'])
            session['accountid'] = result[0][0]
        else:
            # Failed login
            session.pop('account', default=None)
            return render_template('login.html')
        return redirect(url_for('homepage'))
    # GET method: Show loginpage
    return render_template('login.html')

# Logout process (no actual page)
@app.route('/logout')
def logout():
    # Clear the login stored in the session object
    session.pop('account', default=None)
    session.pop('accounttyp', default=None)
    session.pop('accountid', default=None)
    return redirect(url_for('homepage'))

# Create Task Page
@app.route("/createtask", methods=['GET', 'POST'])
def createtask():
    # Post method: save new task and open the homepage
    if request.method == 'POST':
        db = database.Database()
        result = db.getUserId(session['account'])
        id = result[0][0]
        db.createTask(request.form['text'], request.form['settime'], id, request.form['group'])
        return redirect(url_for('homepage'))
    # GET method
    db = database.Database()
    # Get a list of all groups
    result = db.getAllGroups()
    return render_template('createtask.html', result=result)

# Get taskids from given usergroup
def getUserTasks(usergroups):
    db = database.Database()
    taskIds = []
    result = db.getTasksIdFromGroup(usergroups)
    for i in range(len(result)):
        for j in range(len(result[i])):
            taskIds.append(result[i][j])
    return taskIds

# Get taskids for the current logged in user
def getTasks():
    db = database.Database()
    # Get usergroups
    usergroups = db.getUserGroups(session['accountid'])
    taskIds = []
    # For each group the user is in, get the Taskids
    for i in range(len(usergroups)):
        for j in range(len(usergroups[i])):
            taskIds.append(getUserTasks(usergroups[i][j]))
    result = []
    # For each taskid, get task information
    for i in range(len(taskIds)):
        for j in range(len(taskIds[i])):
            result.append(db.getTask(taskIds[i][j]))
    return result

# Get the groupname from the taskid
def getGroupname(taskid):
    db = database.Database()
    result = db.getGroupname(taskid)
    return result

# Get the username from the userid
def getUsername(userid):
    db = database.Database()
    result = db.getUserName(userid)
    return result

# Get the username from the userid
def getTasktime(userid, taskid):
    db = database.Database()
    result = db.getTaskTime(userid, taskid)
    return result

# Task overview page
@app.route("/overview", methods=['GET', 'POST'])
def overview():
    tasks = getTasks()
    # Get the groupname and teachername and enter them into the tasks list
    for i in range(len(tasks)):
        for j in range(len(tasks[i])):
            inner_list = list(tasks[i][j])
            # Taskid is in field 4
            groupname = getGroupname(tasks[i][j][3])
            # Teacherid is in field 3
            teachername = getUsername(tasks[i][j][2])
            # Get tasktime
            tasktime = getTasktime(session['accountid'], tasks[i][j][3])
            # Add the fields to the list
            inner_list.append(groupname[0][0])
            inner_list.append(teachername[0][0])
            inner_list.append(tasktime[0][0])
            tasks[i] = [inner_list]
    return render_template('overview.html', tasks = tasks)

# Stopwatch page
@app.route("/stopwatch", methods=['POST'])
def stopwatch():
    taskid = request.form.get('taskid')
    return render_template('stopwatch.html', taskid = taskid)

# Save watch time and open overview page
@app.route("/savewatch", methods=['POST'])
def savewatch():
    db = database.Database()
    # Get values from form
    taskid = request.form.get('taskid')
    hours = int(request.form.get('hours_input'))
    minutes = int(request.form.get('minutes_input'))
    seconds = int(request.form.get('seconds_input'))
    # calculate time in seconds
    time = seconds + (minutes * 60) + (hours * 3600)
    db.saveTime(time, taskid, session['accountid'])
    return redirect(url_for('overview'))

# Overview page per Class one Teacher
@app.route("/overviewclassme")
def overviewclassme():
    db = database.Database()
    result = db.getTasksTeacher(session['accountid'])
    return render_template('overviewclassme.html', result= result)

# Overview page per Class one Teacher
@app.route("/overviewclassall")
def overviewclassall():
    db = database.Database()
    result = db.getTasksGroups()
    return render_template('overviewclassall.html', result= result)

# Overview page per Class one Teacher
@app.route("/overviewtask")
def overviewtask():
    db = database.Database()
    result = db.getTasksOverview()
    return render_template('overviewtask.html', result= result)

# Overview page per Class one Teacher
@app.route("/overviewstudentasks")
def overviewstudentasks():
    db = database.Database()
    result = db.getTasksOverviewStudents(session['accountid'])
    return render_template('overviewstudentasks.html', result= result)

# Runs the test enviroment
app.run(host='localhost', port=5000)