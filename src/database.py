import mysql.connector

import db_connection


class Database:
    con = db_connection.Connection()
    
    # Open Database connection
    def connectDB(self):
        
        return mysql.connector.connect(host=self.con.host, user=self.con.user, password=self.con.password, database=self.con.database, port=self.con.port)

    # Get the password for this username
    # TODO: currently password is saved in plain text, needs to change
    def getPassword(self, username: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            SELECT password from ' + self.con.database + '.user \
            WHERE username = "'+ username +'"; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get the usertyp from username
    def getUsertype(self, username: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            select t.Name from ' + self.con.database + '.usertype as t \
            INNER JOIN user as u on t.id = u.usertyp \
            WHERE u.username = "' + username + '" \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get the userid from a username
    def getUserId(self, username: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            SELECT ID from ' + self.con.database + '.user \
            WHERE username = "'+ username +'"; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result

    # Create a new task
    def createTask(self, task: str, time: str, id: int, group: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Create Task
        statement = ' \
            INSERT INTO ' + self.con.database + '.tasks (text, time_guessed, time_real, TEACHERID) \
            VALUES ("'+ str(task) + '", '+ str(time) + ', 0, ' + str(id) + ') \
        '
        cursor.execute(statement)
        db.commit()
        # Get ID of new task
        taskid = cursor.lastrowid
        # Associate task with group
        statement = ' \
            INSERT INTO ' + self.con.database + '.taskingroup (TASKID, GROUPID) values (' + str(taskid) + ', ' + str(group) +') \
        '
        cursor.execute(statement)
        db.commit()
        # Create timetable for each user in group
        statement = ' \
            SELECT USERID FROM ' + self.con.database + '.useringroup WHERE GROUPID = ' + str(group) + ' \
        '
        cursor.execute(statement)
        userids = cursor.fetchall()
        for i in range(len(userids)):
            for j in range(len(userids[i])):
                statement = ' \
                INSERT INTO ' + self.con.database + '.usertasktime (USERID, TASKID) values (' + str(userids[i][j]) + ', ' + str(taskid) +') \
                '
                cursor.execute(statement)
                db.commit()
        # Close DB
        cursor.close()
        db.close()
        return

    # Get usergroups from userid
    def getUserGroups(self, userid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            SELECT GROUPID from ' + self.con.database + '.useringroup \
            WHERE USERID = "'+ str(userid) +'"; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get all taskids for this groupid
    def getTasksIdFromGroup(self, groupid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            SELECT TASKID from ' + self.con.database + '.taskingroup \
            WHERE GROUPID = "'+ str(groupid) +'"; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get database fields for task
    def getTask(self, taskid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement 
        cursor.execute(' \
            SELECT text, time_guessed, TEACHERID, ID from ' + self.con.database + '.tasks \
            WHERE ID = "'+ str(taskid) +'"; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Save the time to task
    def saveTime(self, time: str, taskid: str, userid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            UPDATE ' + self.con.database + '.usertasktime set time = ' + str(time) + ' \
            WHERE TASKID = ' + str(taskid) + '\
            AND USERID = ' + str(userid) + '\
        ')
        db.commit()
        # Close DB
        cursor.close()
        db.close()
        
    
    # Get database fields for task
    def getGroupname(self, taskid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement 
        cursor.execute(' \
            SELECT GROUPID from ' + self.con.database + '.taskingroup \
            WHERE TASKID = "'+ str(taskid) +'"; \
        ')
        groupid = cursor.fetchall()
        cursor.execute(' \
            SELECT name from ' + self.con.database + '.groups \
            WHERE ID = "'+ str(groupid[0][0]) +'"; \
        ')
        result = cursor.fetchall()
        
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get the lastname from a userid
    def getUserName(self, userid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            SELECT lastname from ' + self.con.database + '.user \
            WHERE ID = "'+ str(userid) +'"; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
       
    # Get Tasks Overview for one Userid
    def getTasksTeacher(self, userid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            select g.name, avg(t.time_guessed), avg(ut.time) from ' + self.con.database + '.tasks as t \
            Inner join ' + self.con.database + '.taskingroup as tg on t.ID = tg.TASKID\
            Inner join ' + self.con.database + '.groups as g on tg.GROUPID = g.ID\
            Inner join ' + self.con.database + '.usertasktime as ut on t.ID = ut.TASKID \
            WHERE t.TEACHERID = ' + str(userid) + '\
            GROUP BY g.name\
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get Tasks Overview for all Groups
    def getTasksGroups(self):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement
        cursor.execute(' \
            select g.name, avg(t.time_guessed), avg(ut.time) from ' + self.con.database + '.tasks as t \
            Inner join ' + self.con.database + '.taskingroup as tg on t.ID = tg.TASKID\
            Inner join ' + self.con.database + '.groups as g on tg.GROUPID = g.ID\
            Inner join ' + self.con.database + '.usertasktime as ut on t.ID = ut.TASKID \
            GROUP BY g.name\
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get Tasktime for user
    def getTaskTime(self, userid: str, taskid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement 
        cursor.execute(' \
            SELECT time from ' + self.con.database + '.usertasktime \
            WHERE TASKID = "'+ str(taskid) +'" \
            AND USERID = ' + str(userid) +'; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
        # Get Task Overview
    def getTasksOverview(self):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement 
        cursor.execute(' \
            select t.text, avg(t.time_guessed), avg(ut.time), ANY_VALUE(g.name) from ' + self.con.database + '.tasks as t \
            Inner join ' + self.con.database + '.taskingroup as tg on t.ID = tg.TASKID \
            Inner join ' + self.con.database + '.groups as g on tg.GROUPID = g.ID \
            Inner join ' + self.con.database + '.usertasktime as ut on t.ID = ut.TASKID \
            GROUP BY t.text \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get Task Overview for Students
    def getTasksOverviewStudents(self, userid: str):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement 
        cursor.execute(' \
            select u. lastname, u.firstname, t.text, t.time_guessed, ut.time, g.name from ' + self.con.database + '.tasks as t \
            Inner join ' + self.con.database + '.taskingroup as tg on t.ID = tg.TASKID \
            Inner join ' + self.con.database + '.groups as g on tg.GROUPID = g.ID \
            Inner join ' + self.con.database + '.usertasktime as ut on t.ID = ut.TASKID \
            Inner join ' + self.con.database + '.user as u on ut.USERID = u.ID \
            WHERE t.TEACHERID = ' + str(userid) + ' \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result
    
    # Get all groups
    def getAllGroups(self):
        # Open DB
        db = self.connectDB()
        cursor = db.cursor()
        # Execute SQL statement 
        cursor.execute(' \
            SELECT * from ' + self.con.database + '.groups; \
        ')
        result = cursor.fetchall()
        # Close DB
        cursor.close()
        db.close()
        return result